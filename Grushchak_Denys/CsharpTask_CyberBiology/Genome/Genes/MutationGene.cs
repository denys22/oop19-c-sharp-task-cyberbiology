﻿using Model.Utils;
using System;

namespace Model.Genome.Genes
{
    /**
     * <summary>
     * Mutation gene in some cases changes one gene in genome of cell or changes size of genome.
     * </summary>
     */
    public class MutationGene : IGene
    {
        private static readonly float PROBABILITY_GENOME_SIZE_CHANGE = 0.2f;
        private readonly Random rand = new Random();
        private readonly float mutatioinRate;

        /**
         * <param name="mutationRate">mutationRate a mutation rate during reproduction.</param> 
         */
    public MutationGene(in float mutationRate) : base()
        {
            this.mutatioinRate = mutationRate;
        }
        public void Launch(in ICellStandard cell)
        {
            if (rand.NextDouble() < this.mutatioinRate)
            {
                if (rand.NextDouble() < PROBABILITY_GENOME_SIZE_CHANGE)
                {
                    //with the big genome will be big change.
                    int numNewGenes = rand.Next((int)Math.Round(Math.Sqrt((float)cell.GetGenome().Count))) + 1;
                    try
                    {
                        cell.ChangeSideLength(rand.NextDouble() < 0.5F ? numNewGenes : -numNewGenes);
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        Mutate(cell);
                    }
                }
                else
                {
                    Mutate(cell);
                }
            }
        }

        private void Mutate(in ICellStandard cell)
        {
            int position = rand.Next(cell.GetNumberOfGenes());
            int newGeneIndex = rand.Next(cell.GetGenome().Count);
            cell.MutateGene(position, newGeneIndex);
        }

        public string GetDescription()
        {
            return "Mutation";
        }

    }
    
}
