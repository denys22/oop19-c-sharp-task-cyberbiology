﻿using Model.Utils;
using System;

namespace Model.Genome.Genes
{
    /**
     * <summary>
     * The photosynthesis gene give to a cell energy if it is in the sun penetration zone.
     * </summary>
     */
    public class PhotosynthesisGene : IGene
    {
        private readonly int worldHeight;
        private readonly int sunEnergy;
        private readonly float sunPenetration;

        /**
         * <param name="worldHeight">worldHeight the world height.</param> 
         * <param name="sunPenetration"> sunPenetration at what depth cells can perform photosynthesis.</param> 
         * <param name="sunEnergy"> sunEnergy how effective is photosynthesis.</param> 
         */
    public PhotosynthesisGene(in int worldHeight,
                                  in float sunPenetration,
                                  in int sunEnergy)
        {
            this.worldHeight = worldHeight;
            this.sunPenetration = sunPenetration;
            this.sunEnergy = sunEnergy;
        }

        public void Launch(in ICellStandard cell)
        {
            float depth = (float)cell.GetY() / (float)this.worldHeight;
            if (depth <= this.sunPenetration)
            {
                float relativeDepth = depth / this.sunPenetration;
                int energy = (int)Math.Round((1 - relativeDepth) * sunEnergy);
                cell.IncrementEnergy(energy, EnergyTypeEnum.PHOTOSYNTHESIS);
            }
        }

        public string GetDescription()
        {
            return "Photosynthesis";
        }
    }
}
