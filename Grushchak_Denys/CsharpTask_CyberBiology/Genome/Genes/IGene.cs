﻿using Model.Utils;

namespace Model.Genome.Genes
{
    /**
     * 
     * <summary>A basic interface for all genes.</summary>
     *
     */
    public interface IGene
    {
        /**
         * <summary>Launches the functions of gene.</summary>
         * <param name="cell"> cell that launches the gene.</param>
         */
        void Launch(in ICellStandard cell);

        /**
         *<returns>Description of gene.</returns>
         */
        string GetDescription();

    }
}
