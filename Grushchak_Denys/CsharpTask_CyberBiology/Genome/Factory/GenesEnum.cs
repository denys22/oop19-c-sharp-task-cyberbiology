﻿namespace Model.Genome.Factory
{
    /**
     * 
     * <summary>Each enum represent one gene.</summary>
     *
     */
    public enum GenesEnum
    {
        /**
        * <summary>Photosynthesis.</summary>
        */
        PHOTOSYNTHESIS,
        /**
         * <summary>Mutation.</summary>
         */
        MUTATION
    }
}
