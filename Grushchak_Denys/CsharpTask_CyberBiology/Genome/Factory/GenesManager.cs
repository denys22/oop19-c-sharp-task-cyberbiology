﻿using Model.Genome.Genes;
using System;
using System.Collections.Generic;

namespace Model.Genome.Factory
{
    /**
     * 
     * <summary>This instance allows fast access to created genes.</summary>
     *
     */
    public class GenesManager : IGenesManager
    {
        private readonly IDictionary<GenesEnum, IGene> map;

        /**
         * <param name="factory"> factory that can create different genes.</param>
         */
        public GenesManager(in IGenesFactory factory)
        {
            map = new Dictionary<GenesEnum, IGene>
            {
                {GenesEnum.PHOTOSYNTHESIS, factory.CreatePhotosynthesisGene() },
                {GenesEnum.MUTATION, factory.CreateMutationGene() }
            };

        }

        public IGene GetGene(GenesEnum geneEnum)
        {
            IGene gene;
            if(!map.TryGetValue(geneEnum, out gene))
            {
                throw new ArgumentException("The key " + geneEnum + " is not present in map");
            }
            return gene;
        }
    }
}
