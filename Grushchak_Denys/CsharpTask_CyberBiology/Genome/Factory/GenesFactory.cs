﻿using Model.Genome.Genes;

namespace Model.Genome.Factory
{
    public class GenesFactory : IGenesFactory
    {
        private readonly int worldHeight;
        private readonly int sunEnergy;
        private readonly float sunPenetration;
        private readonly float mutationRate;

        public GenesFactory(){
            this.worldHeight = 100;
            this.sunEnergy = 20;
            this.sunPenetration = 0.7F;
            this.mutationRate = 0.2F;
        }

        public MutationGene CreateMutationGene()
        {
            return new MutationGene(this.mutationRate);
        }

        public PhotosynthesisGene CreatePhotosynthesisGene()
        {
            return new PhotosynthesisGene(this.worldHeight, this.sunPenetration, this.sunEnergy);
        }
    }
}
