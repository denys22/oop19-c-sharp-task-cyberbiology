﻿using Model.Genome.Genes;

namespace Model.Genome.Factory
{
    /**
     * 
     * <summary>The Factory for creating genes.</summary>
     *
     */
    public interface IGenesFactory
    {

        /**
         * <returns> a new <see cref="PhotosynthesisGene"/> </returns> 
         */
        PhotosynthesisGene CreatePhotosynthesisGene();

        /**
         * <returns> a new <see cref="MutationGene"/>.</returns> 
         */
        MutationGene CreateMutationGene();
    }
}
