﻿using Model.Genome.Genes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Model.Genome.Factory
{
    /**
     * 
     * <summary>A interface for handle created genes, and offer access to them.</summary>
     *
     */
    public interface IGenesManager
    {
        /**
         * <param name="geneEnum">geneEnum one {@link GenesEnum} that represent a gene.</param> 
         * <returns>the gene that is associated with enum given in input.</returns>
         * <exception cref="ArgumentException">if enum of input is not associated with a gene.</exception> 
         */
        IGene GetGene(GenesEnum geneEnum);

        /**
         * <param name="geneEnumList">geneEnumList a list of {@link GenesEnum}.</param> 
         * <returns>the list with {@link Gene}s that are associated with GenesEnums.</returns> 
         */
        List<IGene> GetGenesList(in List<GenesEnum> geneEnumList)
        {
            return geneEnumList.Select(m => GetGene(m)).ToList();
        }
    }   

}
