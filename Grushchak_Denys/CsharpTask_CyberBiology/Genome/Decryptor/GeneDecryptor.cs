﻿using Model.Genome.Factory;
using Model.Genome.Genes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Model.Genome.Decryptor
{
    /**
     * <summary>
     * GeneDecryptor offers access to the gene. Allow get the genes with the indexes or <see cref="GenesEnum"/>'s. 
     * </summary>
     */
    public class GeneDecryptor : IGeneDecryptor
    {
        /**
         * <summary>A genome map from an index generated random to the correspond gene.</summary>
         */
        private readonly IDictionary<int, IGene> genomeMap;
        /**
         * <summary>Allows get genes of GenesEnum's.</summary>
         */
        private readonly IGenesManager genesManager;


        /**
         * <param name="numberOfGenes"> the number of possible genes.</param>
         * <param name="genesManager"> manager that can give access to genes.</param>
         */
        public GeneDecryptor(in int numberOfGenes, in GenesManager genesManager)
        {
            this.genesManager = genesManager;
            this.genomeMap = new InitializeGenomeMap(numberOfGenes).CreateMap(genesManager);
        }

        public IGene GetGeneOfEnum(GenesEnum geneEnum)
        {
            return genesManager.GetGene(geneEnum);
        }

        public IGene GetGeneOfIndex(int index)
        {
            IGene gene;
            Check(!genomeMap.TryGetValue(index, out gene));
            return gene;
        }

        // this method is useful for creating a first cell.
        public int GetIndexOfGene(GenesEnum geneEnum)
        {
            Nullable<int> index = new HashSet<KeyValuePair<int, IGene>>(genomeMap)
                                        .Where(f => f.Value.Equals(GetGeneOfEnum(geneEnum)))
                                        .Select(m => m.Key)
                                        .FirstOrDefault();
            Check(index == null);
            return index.Value;

        }

        public bool IsGenePresent(int index)
        {
            return genomeMap.ContainsKey(index);
        }

        private void Check(in bool exception)
        {
            if (exception)
            {
                throw new ArgumentException("Invalid argument");
            }
        }

        /**
         * 
         * <summary>A class for a genome map creation and initialization.</summary>
         *
         */
        private class InitializeGenomeMap
        {
            // The list with all possible indexes
            private readonly List<int> indexes;
            private readonly Random random = new Random();

            /**
             * <param name="numberOfGenes"> the number of possible genes.</param>
             */
            internal InitializeGenomeMap(int numberOfGenes)
            {
                this.indexes = Enumerable.Range(0, numberOfGenes).ToList();
            }

            /**
             * <summary>The method for creating new genome map.</summary>
             * 
             * <returns>genome map.</returns> 
             */
            internal IDictionary<int, IGene> CreateMap(IGenesManager genesManager)
            {
                // make the list with all possible genes enums.
                List<GenesEnum> enums = new List<GenesEnum>(Enum.GetValues(typeof(GenesEnum))
                                                                    .OfType<GenesEnum>()
                                                                    .ToList());

                //add some copies of gene enums. So some genes will be appear more frequently.
                //for (int i = 0; i < 2; i++)
                //{
                //    enums.Add(GenesEnum.LOOK_AHEAD);
                //    enums.Add(GenesEnum.TURN_FROM_CURRENT_DIRECTION);
                //    enums.Add(GenesEnum.MOVEMENT);

                return genesManager.GetGenesList(enums).ToDictionary(a => GetRandomIndex(), b => b);
            }

            /**
             * <summary>Extract casual index.</summary>
             * 
             * <returns>index for a gene.</returns> 
             * <exception cref="InvalidOperationException">
             *  If is requested more indexes than the number of genes defined in the constructor.
             * </exception>
             */
            private int GetRandomIndex()
            {
                if (!indexes.Any())
                {
                    throw new InvalidOperationException("There are not free indexes for genes");
                }
                int index = random.Next(indexes.Count());
                int value = indexes[index];
                indexes.RemoveAt(index);
                return value;
            }
        }
    }
}
