﻿using Model.Genome.Factory;
using Model.Genome.Genes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Genome.Decryptor
{
    /**
     * 
     * <summary>A gene decryptor offers access to the genes.</summary>
     *
     */
    public interface IGeneDecryptor
    {
        /**
         * <param name="index">index of a gene.</param> 
         * <returns>A gene that correspond to the index.</returns>
         * <exception cref="ArgumentException">if the index has not corresponded gene.</exception>
         */
        IGene GetGeneOfIndex(int index);

        /**
         * <summary>Controls if index has a correspond gene.</summary>
         * <param name="index">index of a possible gene.</param>
         * <returns>true if the index has a corresponded gene.</returns>
         */
        bool IsGenePresent(int index);

        /**
         * <summary>Return index of gene.</summary>
         * <param name="gene">gene an enum that represent a gene.</param>
         * <returns>index of gene associated with the enum. If the gene has some indexes then first found will be returned.</returns>
         * <exception cref="ArgumentException"> if the gene has not an index.</exception>
         */
        int GetIndexOfGene(GenesEnum gene);

        /**
         * <param name="geneEnum"> an enum that correspond to an gene.</param>
         * <returns>a gene that correspond to the input enum.</returns>
         */
        IGene GetGeneOfEnum(GenesEnum geneEnum);
    }
}
