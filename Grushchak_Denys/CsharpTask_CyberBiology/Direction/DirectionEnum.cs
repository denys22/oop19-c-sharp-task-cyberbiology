﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Model.Direction
{
    /**
    * <summary>All possible directions. Each enum contains the relative coordinates of the direction.</summary>
    */
    public enum Direction
    {
        /**
         * <summary>Direction to east(1, 0).<summary>
         */
        EAST,
        /**
         * <summary>Direction to northeast (1, -1).<summary>
         */
        NORTHEAST,
        /**
         * <summary>Direction to north (0, -1).<summary>
         */
        NORTH,
        /**
         * <summary>Direction to northwest (-1, -1).<summary>
         */
        NORTHWEST,
        /**
         * <summary>Direction to west (-1, 0).<summary>
         */
        WEST,
        /**
         * <summary>Direction to southwest (-1, 1).<summary>
         */
        SOUTHWEST,
        /**
         * <summary>Direction to south (0, 1).<summary>
         */
        SOUTH,
        /**
         * <summary>Direction to southeast (1, 1).<summary>
         */
        SOUTHEAST
    }

    /**
     * <summary>An extention for Direction, needed for implement methods of the java enums in C# </summary>
     */
    public static class DirectionExtention
    {
        public static readonly int Size = Enum.GetNames(typeof(Direction)).Length;

        /**
         * <summary>The map from index to direction.</summary>
         */
        private static readonly IDictionary<int, Direction> map = Enum.GetValues(typeof(Direction))
                                                                      .Cast<Direction>()
                                                                      .Select(l => new Tuple<int, Direction>(l.GetIndex(), l))
                                                                      .ToDictionary(key => key.Item1, value => value.Item2);
        

        /**
         * <summary>Return direction of the index</summary>
         * <param name="index">index of direction</param>
         * <returns>the direction of this index</returns>
         * <exception cref="ArgumentException">if the index does not correspond to any dirction</exception>
         */
        public static Direction GetDirection(in int index)
        {
            Direction value;
            if (!map.TryGetValue(index, out value))
            {
                throw new ArgumentException("The index is not correspond to any direction.");

            }
            return value;
        }

        /**
         * <returns>movement along the X axis.</returns> 
         */
        public static int MovementAlongX(this Direction direction)
        {
            switch (direction)
            {
                case Direction.EAST: case Direction.NORTHEAST: case Direction.SOUTHEAST: 
                    return 1;
                case Direction.NORTH: case Direction.SOUTH:
                    return 0;
                case Direction.WEST: case Direction.NORTHWEST: case Direction.SOUTHWEST:
                    return -1;
                default:
                    throw new ArgumentOutOfRangeException("direction");

            }
        }


        /**
        * <returns>movement along the Y axis.</returns>
        */
        public static int MovementAlongY(this Direction direction)
        {
            switch (direction)
            {
                case Direction.SOUTH: case Direction.SOUTHWEST: case Direction.SOUTHEAST:
                    return 1;
                case Direction.EAST: case Direction.WEST:
                    return 0;
                case Direction.NORTH: case Direction.NORTHWEST: case Direction.NORTHEAST:
                    return -1;
                default:
                    throw new ArgumentOutOfRangeException("direction");

            }
        }

        /**
        * <summary>Give index of direction</summary>
        * <param name="direction">an direction</param>
        * <returns> the index of direction </returns>
        */
        public static int GetIndex(this Direction direction)
        {
            return (int)direction;
        }
    }
}

