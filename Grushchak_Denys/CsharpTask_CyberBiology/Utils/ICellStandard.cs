﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Utils
{
    public interface ICellStandard
    {
        public List<int> GetGenome();

        public int GetNumberOfGenes();

        public void MutateGene(int position, int newGeneIndex);

        public void ChangeSideLength(int sizeOfChange);

        public int GetY();

        public void IncrementEnergy(int energy, EnergyTypeEnum type);
    }
}
