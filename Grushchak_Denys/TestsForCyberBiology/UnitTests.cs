using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model.Direction;
using Model.Genome.Decryptor;
using Model.Genome.Factory;
using Model.Genome.Genes;
using System;

namespace TestsForCyberBiology
{

    /**
     * 
     * <summary>Simple tests for directions methods.</summary>
     *
     */
    [TestClass]
    public class UnitTests
    {
        /**
        * <summary>DirectionDecriptor test.</summary>
        */
        [TestMethod]
        public void TestDirectionDecriptor()
        {
            int i = 0;
            Assert.AreEqual(DirectionDecryptor.GetDirection(i--), Direction.EAST);
            Assert.AreEqual(DirectionDecryptor.GetDirection(i), Direction.SOUTHEAST);
            i = i - DirectionExtention.Size;
            Assert.AreEqual(DirectionDecryptor.GetDirection(i++), Direction.SOUTHEAST);
            Assert.AreEqual(DirectionDecryptor.GetDirection(i), Direction.EAST);
            i = i + DirectionExtention.Size;
            Assert.AreEqual(DirectionDecryptor.GetDirection(i++), Direction.EAST);
            Assert.AreEqual(DirectionDecryptor.GetDirection(i++), Direction.NORTHEAST);
            Assert.AreEqual(DirectionDecryptor.GetDirection(i++), Direction.NORTH);
            Assert.AreEqual(DirectionDecryptor.GetDirection(i++), Direction.NORTHWEST);
            Assert.AreEqual(DirectionDecryptor.GetDirection(i++), Direction.WEST);
            Assert.AreEqual(DirectionDecryptor.GetDirection(i++), Direction.SOUTHWEST);
            Assert.AreEqual(DirectionDecryptor.GetDirection(i++), Direction.SOUTH);
            Assert.AreEqual(DirectionDecryptor.GetDirection(i++), Direction.SOUTHEAST);
            Assert.AreEqual(DirectionDecryptor.GetDirection(i), Direction.EAST);
        }

        [TestMethod]
        public void TestDirection()
        {
            Assert.IsTrue(Direction.EAST.MovementAlongX() == 1);
            Assert.IsTrue(Direction.EAST.MovementAlongY() == 0);
            Assert.IsTrue(Direction.SOUTHWEST.MovementAlongY() == 1);
            Assert.IsTrue(Direction.SOUTHWEST.MovementAlongX() == -1);
        }

        [TestMethod]
        public void TestModelGenome()
        {
            IGeneDecryptor decryptor = new GeneDecryptor(5, new GenesManager(new GenesFactory()));
            Assert.IsTrue(decryptor.GetGeneOfEnum(GenesEnum.MUTATION) is MutationGene);
            Assert.IsTrue(decryptor.GetGeneOfEnum(GenesEnum.PHOTOSYNTHESIS) is PhotosynthesisGene);
            int mutationIndex = decryptor.GetIndexOfGene(GenesEnum.MUTATION);
            int photosIndex = decryptor.GetIndexOfGene(GenesEnum.PHOTOSYNTHESIS);
            Assert.IsTrue(decryptor.GetGeneOfIndex(mutationIndex) is MutationGene);
            Assert.IsTrue(decryptor.IsGenePresent(mutationIndex));
            int newIndex = ++mutationIndex;
            if(newIndex == photosIndex)
            {
                newIndex++;
            }
            Assert.IsFalse(decryptor.IsGenePresent(newIndex));
            try
            {
                decryptor.GetGeneOfIndex(newIndex);
                Assert.Fail();
            }
            catch (ArgumentException) { }

            try
            {
                new GeneDecryptor(1, new GenesManager(new GenesFactory()));
                Assert.Fail();
            }
            catch (InvalidOperationException) { }
        }
    }
}
