﻿using System.Drawing;

namespace Model
{
    /// <summary>
    /// Class that manages the colors of the standard entities of the program.
    /// </summary>
    public class ColorProgramValue
    {
        /// <summary>
        /// Enum collecting color types related to the entity.
        /// </summary>
        public enum ColorProgram
        {
           BACKGROUND_COLOR, CELL_DEATH_COLOR, STONE_COLOR
        }

        /// <summary>
        /// Method that returns the color relative to a ColorProgram type.
        /// </summary>
        /// <param name="c">Type of ColorProgram you want to get the color of. </param>
        /// <returns>Color relative to the type inserted.</returns>
        public static Color GetColor(ColorProgram c)
        {
            return c switch
            {
                ColorProgram.BACKGROUND_COLOR => Color.FromArgb(225, 225, 225),
                ColorProgram.CELL_DEATH_COLOR => Color.FromArgb(192, 192, 192),
                ColorProgram.STONE_COLOR => Color.FromArgb(95, 95, 95),
                _ => throw new System.ArgumentException("TIPO DI COLORE INESISTENTE"),
            };
        }
    }

    
}