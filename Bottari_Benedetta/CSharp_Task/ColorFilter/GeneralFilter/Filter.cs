﻿using System.Drawing;

namespace Model
{
    /// <summary>
    /// Standard interface that allows you to obtain the colors of the implemented entities.
    /// </summary>
    public interface IFilter
    {
        /// <summary>
        /// Method that returns the color relative to the passed square.
        /// </summary>
        /// <param name="square">Part of the world whose color you want to identify the color.</param>
        /// <returns>Color relative to the indicated square</returns>
        Color GetColor(Square square);
    }
}
