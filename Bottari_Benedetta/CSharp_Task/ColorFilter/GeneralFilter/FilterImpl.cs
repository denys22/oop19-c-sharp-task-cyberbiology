﻿using System;
using System.Drawing;
using static Model.ColorProgramValue;

namespace Model
{
    /// <summary>
    /// Abstract class that implements only the default colors of standard entities .
    /// </summary>
    public abstract class FilterImpl : IFilter
    {
        public Color GetColor(Square square)
        {
            Color color;
            if (square.IsPresent)
            {
                color = ColorEntity(square.Entity);
            }
            else
            {
                color = ColorProgramValue.GetColor(ColorProgram.BACKGROUND_COLOR);
            }
            return color;
        }

        private Color ColorEntity(Entity entity)
        {
            var entityColor = entity.EType switch
            {
                EntityType.CELL => CellColor(entity.Cell),
                EntityType.STONE => ColorProgramValue.GetColor(ColorProgram.STONE_COLOR),
                EntityType.EMPTY => throw new ArgumentException("TIPO DI ENTITY INESISTENTE"),
                _ => throw new System.ArgumentException("TIPO DI ENTITY INESISTENTE"),
            };
            return entityColor;
        }

        private Color CellColor(Cell cell)
        {
            var cellColor = cell.Type switch
            {
                CellType.CELL_STANDARD_ALIVE => GetCellStandardColor(cell.CellStandard),
                CellType.CELL_DEAD => ColorProgramValue.GetColor(ColorProgram.CELL_DEATH_COLOR),
                CellType.NO_CELL => throw new ArgumentException("TIPO DI CELL INESISTENTE"),
                _ => throw new ArgumentException("TIPO DI CELL INESISTENTE"),
            };
            return cellColor;
        }

        protected abstract Color GetCellStandardColor(CellStandard cellStandard);
    }
}