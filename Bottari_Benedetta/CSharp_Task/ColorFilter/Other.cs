﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Types of entities
    /// </summary>
    public enum EntityType { CELL, STONE, EMPTY }

    /// <summary>
    /// Cell types
    /// </summary>
    public enum CellType { CELL_STANDARD_ALIVE, CELL_DEAD, NO_CELL }

    /// <summary>
    /// Class that implements a portion of the world in which an entity can stay.
    /// </summary>
    public class Square
    {
        public Entity? Entity { get; }
        public EntityType EntityType { get; }

        /// <summary>
        /// Builder of a portion of land.
        /// </summary>
        /// <param name="type">Type of entity residing in that square.</param>
        /// <param name="cellType">Type of cell residing in that square.</param>
        public Square(EntityType type, CellType cellType) {
            EntityType = type;
            Entity = new Entity(type, cellType);
        }

        /// <summary>
        /// Method that returns if an entity resides in the square.
        /// </summary>
        public bool IsPresent => !EntityType.Equals(EntityType.EMPTY);
    }

    /// <summary>
    /// Class that implements an entity that may exist in the world.
    /// </summary>
    public class Entity
    {
        public EntityType EType { get; }

        public Cell? Cell { get; }

        /// <summary>
        /// Builder of an entity.
        /// </summary>
        /// <param name="type">Type of entity representing.</param>
        /// <param name="cellType">Type of cell to which the entity belongs.</param>
        public Entity(EntityType type, CellType cellType)
        {
            EType = type;
            Cell = new Cell(cellType);
        }
    }

    /// <summary>
    /// Class implementing an entity type corresponding to a cell of any type.
    /// </summary>
    public class Cell
    {
        public CellType Type { get; }
        public CellStandard? CellStandard { get; }

        /// <summary>
        /// Cell Builder.
        /// </summary>
        /// <param name="type">Type of cell representing.</param>
        public Cell(CellType type)
        {
            Type = type;
            CellStandard = new CellStandard();
        }
    }

    /// <summary>
    /// Class implementing a living cell type.
    /// </summary>
    public class CellStandard
    {
        private int altruism;
        private int eating;
        private int photosynthesis;
        private int coverting_Mineral;
        private int energyTot;

        public int EnergyTot => energyTot;
        public int Altruism => altruism;
        public int Eating => eating;
        public int Photosynthesis => photosynthesis;
        public int Coverting_Mineral => coverting_Mineral;

        /// <summary>
        /// Builder of a standard living cell.
        /// </summary>
        public CellStandard()
        {
            altruism = 0;
            eating = 0;
            photosynthesis = 0;
            coverting_Mineral = 0;
            energyTot = altruism + eating + photosynthesis + coverting_Mineral;
        }

        /// <summary>
        /// Method for sectarian energy values stored by the cell.
        /// </summary>
        /// <param name="a">Value of stored energy, released by other cells.</param>
        /// <param name="e">Value of stored energy taken from other cells.</param>
        /// <param name="p">Value of stored energy, taken from photosynthesis</param>
        /// <param name="m">Value of stored energy, taken from minerals</param>
        public void SetValue(int a, int e, int p, int m)
        {
            altruism = a;
            eating = e;
            photosynthesis = p;
            coverting_Mineral = m;
            energyTot = altruism + eating + photosynthesis + coverting_Mineral;
        }
    }
}
