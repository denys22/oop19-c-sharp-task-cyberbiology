﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Abstract class that implements the colors of living cells based on the type of nutrient absorbed during their lifetime.
    /// </summary>
    public class NutritionFilter : FilterImpl
    {
        private readonly int MAX_RANGE_COLOR = 225;
        private int r;
        private int g;
        private int b;

        /// <summary>
        /// Method that restores the color of CellStandard:
        /// RED colour if you have absorbed energy from OTHER CELL(for altruism or cannibalism);
        /// GREEN colour if you have absorbed energy through PHOTOSYNTHESIS;
        /// BLUE if you have absorbed MINERAL energy;
        /// WHITE if the cell has just been born and has not yet absorbed energy;
        /// MIXED colour with the highest percentage of shade according to the one absorbed during its life;
        /// </summary>
        /// <param name="cellStandard">Cell you want to analyze.</param>
        /// <returns>Colour relative to the cell.</returns>
        protected override Color GetCellStandardColor(CellStandard cellStandard)
        {
            int energyTot = cellStandard.EnergyTot;
            if (energyTot == 0)
            {
                return Color.FromArgb(MAX_RANGE_COLOR, MAX_RANGE_COLOR, MAX_RANGE_COLOR);
            }
            else
            {
                SetValue(cellStandard);
                return Color.FromArgb(GetValue(r, energyTot), GetValue(g, energyTot), GetValue(b, energyTot));
            }
        }

        private void SetValue(CellStandard cellstandard)
        {
            r = cellstandard.Altruism + cellstandard.Eating;
            g = cellstandard.Photosynthesis;
            b = cellstandard.Coverting_Mineral;
        }

        private int GetValue(int val, int energyTot) => val * MAX_RANGE_COLOR / energyTot;
    }
}
