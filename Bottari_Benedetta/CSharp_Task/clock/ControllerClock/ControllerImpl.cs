﻿using System;
using System.Threading;

namespace Model
{
    /// <summary>
    /// Class that implements the controller that manages the program clock.
    /// </summary>
    public class Controller : IController
    {
        private static LogicClock time ;
        private static readonly int MILLS_PAUSE = 40;
        private readonly int LastTime;
        public int Cont { get; private set; } = -1;

        /// <summary>
        /// Builder of the class that implements the controller.
        /// </summary>
        /// <param name="lastTime">Number of clock updates that the controller must manage.</param>
        public Controller(int lastTime)
        {
            LastTime = lastTime;
        }
        
        void IController.Start()
        {
            int thisSecond;
            int lastSecond = 1;
            time = new LogicClock();
            while (Cont < LastTime)
            {
                thisSecond = time.IntSecond;
                if (thisSecond != lastSecond)
                {
                    lastSecond = thisSecond;
                    Console.WriteLine(time.Minute + " : " + time.Second + " =>" + Cont);
                    Cont++;
                }
                Thread.Sleep(MILLS_PAUSE);
            }
        }
    }
}
