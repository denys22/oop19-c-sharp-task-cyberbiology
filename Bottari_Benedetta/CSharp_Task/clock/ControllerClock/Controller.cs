﻿namespace Model
{
    /// <summary>
    /// Standard interface that manages the clock control.
    /// </summary>
    public interface IController
    {
        /// <summary>
        /// Method that starts the controller.
        /// </summary>
        void Start();

        /// <summary>
        /// Property that returns the number of updates performed by the clock up to that time.
        /// </summary>
        int Cont { get; }
    }
}
