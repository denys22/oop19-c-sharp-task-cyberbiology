﻿using System;

namespace Model
{
    /// <summary>
    /// Class that implements a clock that starts counting time from when it is created.
    /// </summary>
    internal class LogicClock : ILogicClock
    {
        private readonly DateTime start;
        private readonly int SECONDS_IN_MINUTE = 60;

        /// <summary>
        /// Builder that initializes the starting time of the watch at the current time.
        /// </summary>
        public LogicClock()
        {
            start = DateTime.Now;
        }

        public string Second => IntSecond.ToString("00");

        public string Minute => GetIntMinute().ToString("00");

        public int IntSecond => (DateTime.Now - start).Seconds % SECONDS_IN_MINUTE;

        private int GetIntMinute()
        {
            DateTime current = DateTime.Now;
            return (current - start).Minutes;
        }
    }
}
