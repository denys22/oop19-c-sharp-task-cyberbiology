﻿using System;

namespace Model
{
    /// <summary>
    /// Standard interface for the physical implementation of the clock.
    /// </summary>
    interface ILogicClock
    {
        /// <summary>
        /// Method that returns a string in double-digit format containing the number of seconds elapsed since the last 
        /// minute was initialized.
        /// </summary>
        String Second { get; }

        /// <summary>
        /// Method that returns a string in double-digit format containing the number of minutes elapsed since the clock was initialized.
        /// </summary>
        String Minute { get; }

        /// <summary>
        /// Method that returns the number of seconds elapsed since the last minute was initialized.
        /// </summary>
        int IntSecond { get; }
    }
}
