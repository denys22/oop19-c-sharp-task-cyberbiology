using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace CSharp_Test
{
    ///<summary>
    ///Simple tests for Clock
    ///to test whether the seconds elapsed are the actual ones.
    ///</summary>
    [TestClass]
    public class ClockTest
    {
        /// <summary>
        /// Method that tests the passing of 1 SECOND.
        /// </summary>
        [TestMethod]
        public void TestClockOne()
        {
            IController c;

            c = new Controller(1);
            c.Start();
            Assert.AreEqual(1, c.Cont);
        }

        /// <summary>
        /// Method that tests the passing of 2 SECOND.
        /// </summary>
        [TestMethod]
        public void TestClockTwo()
        {
            IController c;

            c = new Controller(2);
            c.Start();
            Assert.AreEqual(2, c.Cont);
        }

        /// <summary>
        /// Method that tests the passing of 3 SECOND. 
        /// </summary>
        [TestMethod]
        public void TestClockThree()
        {
            IController c;

            c = new Controller(3);
            c.Start();
            Assert.AreEqual(3, c.Cont);
        }

        /// <summary>
        /// Method that tests the passing of 5 SECOND.
        /// </summary>
        [TestMethod]
        public void TestClockFive()
        {
            IController c;

            c = new Controller(5);
            c.Start();
            Assert.AreEqual(5, c.Cont);
        }

        /// <summary>
        /// Method that tests the passing of 30 SECOND.
        /// </summary>
        [TestMethod]
        public void TestClockThirty()
        {
            IController c;

            c = new Controller(30);
            c.Start();
            Assert.AreEqual(30, c.Cont);
        }
    }
}

