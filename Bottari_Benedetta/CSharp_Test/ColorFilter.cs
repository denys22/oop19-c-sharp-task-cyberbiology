using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using System.Drawing;
using static Model.ColorProgramValue;

namespace CSharp_Test
{
    ///<summary>
    ///Class that tests the color of the nutrition filter entities.
    ///</summary>

    [TestClass]
    public class ColorFilterTest
    {
        private readonly Model.IFilter nutritionFilter = new Model.NutritionFilter();

        ///<summary>
        ///Method that tests the color of EMPTY SQUARE, STONE ENTITY, CELL DEAD.
        ///</summary>
        [TestMethod]
        public void TestStandardColor()
        {
            Square squareEmpty = new Square(EntityType.EMPTY, CellType.NO_CELL);
            Assert.AreEqual(ColorProgramValue.GetColor(ColorProgram.BACKGROUND_COLOR), nutritionFilter.GetColor(squareEmpty), "The color should be BACKGROUND_COLOR");

            Square squareStone = new Square(EntityType.STONE, CellType.NO_CELL);
            Assert.AreEqual(ColorProgramValue.GetColor(ColorProgram.STONE_COLOR), nutritionFilter.GetColor(squareStone), "The color should be STONE_COLOR");

            Square squareCellDeath = new Square(EntityType.CELL, CellType.CELL_DEAD);
            Assert.AreEqual(ColorProgramValue.GetColor(ColorProgram.CELL_DEATH_COLOR), nutritionFilter.GetColor(squareCellDeath), "The color should be CELL_DEATH_COLOR");
        }

        ///<summary>
        ///Method that tests the color of CELL ALIVE sas soon as they are BORN, therefore with energy levels all equal to 0.
        ///</summary>
        [TestMethod]
        public void TestColorCellBorn()
        {
            Square squareCellAlive = new Square(EntityType.CELL, CellType.CELL_STANDARD_ALIVE);
            Assert.AreEqual(Color.FromArgb(225, 225, 225), nutritionFilter.GetColor(squareCellAlive), "The color should be WHITE");
        }

        ///<summary>
        ///Method that tests the color of LIVE CELL that have absorbed energy only from PHOTOSYNTHESIS.
        ///</summary>
        [TestMethod]
        public void TestColorCellPhotosintesi()
        {
            Square squareCellAlive = new Square(EntityType.CELL, CellType.CELL_STANDARD_ALIVE);

            squareCellAlive.Entity.Cell.CellStandard.SetValue(0,0,10,0);
            Assert.AreEqual(Color.FromArgb(0, 225, 0), nutritionFilter.GetColor(squareCellAlive), "The color should be GREEN");
            Assert.AreNotEqual(Color.FromArgb(225, 0, 0), nutritionFilter.GetColor(squareCellAlive), "The color should not be RED");
            Assert.AreNotEqual(Color.FromArgb(0, 0, 225), nutritionFilter.GetColor(squareCellAlive), "The color should not be BLUE");
        }

        ///<summary>
        ///Method that tests the color of LIVE CELL that have absorbed energy only from MINERALS.
        ///</summary>
        [TestMethod]
        public void TestColorCellMineral()
        {
            Square squareCellAlive = new Square(EntityType.CELL, CellType.CELL_STANDARD_ALIVE);

            squareCellAlive.Entity.Cell.CellStandard.SetValue(0, 0, 0, 1);
            Assert.AreEqual(Color.FromArgb(0, 0, 225), nutritionFilter.GetColor(squareCellAlive), "The color should be BLU");
            Assert.AreNotEqual(Color.FromArgb(225, 0, 0), nutritionFilter.GetColor(squareCellAlive), "The color should not be RED");
            Assert.AreNotEqual(Color.FromArgb(0, 255, 0), nutritionFilter.GetColor(squareCellAlive), "The color should not be GREEN");
        }

        ///<summary>
        ///Method that tests the color of LIVE CELLS that have absorbed energy only from OTHER CELLS (for altruism or cannibalism).
        ///</summary>
        [TestMethod]
        public void TestColorCellEating()
        {
            Square squareCellAlive = new Square(EntityType.CELL, CellType.CELL_STANDARD_ALIVE);

            squareCellAlive.Entity.Cell.CellStandard.SetValue(5, 0, 0, 0);
            Assert.AreEqual(Color.FromArgb(225, 0, 0), nutritionFilter.GetColor(squareCellAlive), "The color should be RED");
            Assert.AreNotEqual(Color.FromArgb(0, 0, 225), nutritionFilter.GetColor(squareCellAlive), "The color should not be BLU");
            Assert.AreNotEqual(Color.FromArgb(0, 255, 0), nutritionFilter.GetColor(squareCellAlive), "The color should not be GREEN");

            squareCellAlive.Entity.Cell.CellStandard.SetValue(0, 34, 0, 0);
            Assert.AreEqual(Color.FromArgb(225, 0, 0), nutritionFilter.GetColor(squareCellAlive), "The color should be RED");
            Assert.AreNotEqual(Color.FromArgb(0, 0, 225), nutritionFilter.GetColor(squareCellAlive), "The color should not be BLU");
            Assert.AreNotEqual(Color.FromArgb(0, 255, 0), nutritionFilter.GetColor(squareCellAlive), "The color should not be GREEN");

        }

        ///<summary>
        ///Method that tests the color of the LIVE CELL that have absorbed energy from DIFFERENT SOURCES.
        ///</summary>
        [TestMethod]
        public void TestColorCellMixed()
        {
            Square squareCellAlive = new Square(EntityType.CELL, CellType.CELL_STANDARD_ALIVE);
            Color colorCell;
            Assert.AreEqual(Color.FromArgb(225, 225, 225), nutritionFilter.GetColor(squareCellAlive), "The color should be WHITE");

            squareCellAlive.Entity.Cell.CellStandard.SetValue(5, 6, 25, 8);
            colorCell = nutritionFilter.GetColor(squareCellAlive);

            Assert.IsTrue(colorCell.G > colorCell.B, "The color should be more GREEN");
            Assert.IsTrue(colorCell.G > colorCell.R, "The color should be more GREEN");
            Assert.IsTrue(colorCell.R > colorCell.B, "The color should be more RED");

            squareCellAlive.Entity.Cell.CellStandard.SetValue(2, 0, 16, 43);
            colorCell = nutritionFilter.GetColor(squareCellAlive);

            Assert.IsTrue(colorCell.B > colorCell.G, "The color should be more BLUE");
            Assert.IsTrue(colorCell.B > colorCell.R, "The color should be more BLUE");
            Assert.IsTrue(colorCell.G > colorCell.R, "The color should be more RED");

        }
    }
}

