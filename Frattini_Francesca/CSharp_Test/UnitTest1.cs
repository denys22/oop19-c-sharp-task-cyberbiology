    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using Model.World;
    using Model.Code_From_Grushchack_Denys;
    using Model.Entities;
    using Model.Utils;
    using static Model.Entities.Entity;
using static Model.Entities.Cell;

namespace CSharp_Test
    {

        /**
         * 
         * <summary>Simple tests for World</summary>
         *
         */
        [TestClass]
        public class Test
        {
            [TestMethod]
            public void TestDirection()
            {
                Assert.AreEqual(Direction.EAST.MovementAlongX(), 1);
                Assert.AreEqual(Direction.EAST.MovementAlongY(), 0);
                Assert.AreNotEqual(Direction.NORTH.MovementAlongX(), -1);
                Assert.AreEqual(Direction.SOUTH.MovementAlongY(), 1);
                Assert.IsTrue(Direction.SOUTHWEST.MovementAlongX() == -1);
            }

            [TestMethod]
            public void TestModelWorld()
            {
                var h = 50;
                var w = 75;
                var world = new World(w, h);
                var gravity = new Gravity(world);
                var cast = new CastClass();
                Assert.IsFalse(world.IsThereAnything(25, 40));
                world.PutCell(25, 40, new CellAlive(25, 40));
                Assert.AreEqual(world.GetAliveCells(), 1);
                Assert.AreEqual(world.GetWorldHeight(), 50);
                Assert.AreEqual(world.GetWorldWidth(), 75);
                Assert.AreEqual(world.GetType(25, 40), EntityTypeNameEnum.CELL);
                Assert.AreEqual(world.GetType(30, h-1), EntityTypeNameEnum.STONE);
                world.TryMoveCell(25, 40, Direction.NORTHEAST);
                Assert.AreEqual(world.GetType(26, 39), EntityTypeNameEnum.CELL);
                Assert.IsTrue(world.IsThereFreeSpaceAround(26, 39));
                try
                {
                    world.GetCellType(5, 5);
                    Assert.Fail();
                }
                catch(InvalidOperationException) { }
                world.PutChild(26, 39, new CellAlive(27, 39), Direction.EAST);
                world.MakeCellDeath(26, 39);
                gravity.CellFallingDown(cast.CellDeadCast(world.GetSquare(26, 39).GetEntity().Get()));
                Assert.AreEqual(world.GetCellType(26, 40), CellTypeNameEnum.CELL_DEAD);
                Assert.AreEqual(world.GetDeadCells(), 1);
                Assert.IsTrue(world.GetAliveCells() == world.GetDeadCells());
                try
                {
                    world.GetType(25, 45);
                    Assert.Fail();
                }
                catch (InvalidOperationException) {  }
                world.PutCell(10, 10, new CellAlive(10, 10));
                Assert.IsFalse(world.GetAliveCells() == world.GetDeadCells());
                world.RemoveCell(10, 10);
                Assert.AreEqual(world.GetAliveCells(), 1);
                Assert.IsTrue(world.IsThereFreeSpaceAround(10, 10));
            }
        }
    }

