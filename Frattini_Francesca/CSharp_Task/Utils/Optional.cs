﻿using System;
using System.Collections.Generic;
using System.Text;
#nullable enable

namespace Model.Utils
{
    public class Optional<T>
    {
            private T value;
            public bool IsPresent { get; set; } = false;

            private Optional() { }

            public static Optional<T> Empty()
            {
                return new Optional<T>();
            }

            public static Optional<T> Of(T value)
            {
                Optional<T> obj = new Optional<T>();
                obj.Set(value);
                return obj;
            }

            private void Set(T value)
            {
                this.value = value;
                this.IsPresent = true;
            }

            public T Get()
            {
                return value;
            }

        }
    }
