﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Utils
{
    public class Pair<X, Y>
    {
        private X first;
        private Y second;

        public Pair(X first, Y second)
        {
            this.first = first;
            this.second = second;
        }
        public X GetX()
        {
            return this.first;
        }

        public Y GetY()
        {
            return this.second;
        }

        public override string ToString()
        {
            return "<" + first + "," + second + ">";
        }
    }
}

