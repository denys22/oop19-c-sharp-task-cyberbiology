﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Entities;

namespace Model.World
{
    public interface IGravity
    {
     /**
      * 
      *<param name="cell">the dead cell to move down</param>
      * */
        void CellFallingDown(CellDead cell);
    }
}
