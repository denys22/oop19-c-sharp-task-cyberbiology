﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Utils;
using Model.Entities;

namespace Model.World
{
    public interface ISquare
    {
        /**
        * 
        *  <returns>an optional with the entity(cell or stone) if it's present in the square, or null.</returns>
        */
        Optional<Entity> GetEntity();
        /**
         * create a new cell (dead or alive) in that square.
         * <param name="entity">the entity to set in a given square</param> 
         */
        void SetEntity(Optional<Entity> entity);
    }
}
