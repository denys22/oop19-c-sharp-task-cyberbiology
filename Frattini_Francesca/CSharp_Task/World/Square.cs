﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Text;
using Model.Entities;
using Model.Utils;

namespace Model.World
{
    public class Square : ISquare
    {
        /**
     * the occupant of each square.
     */

        private Optional<Entity> entity;

        /**
         * SquareImp constructor.
         * @param entity the entity to put when a square is created
         */
         public Square (Optional<Entity> entity)
        {
            this.entity = entity;
        }
      
    public Optional<Entity> GetEntity() {
            return this.entity;
        }

    public void SetEntity(Optional<Entity> entity)
        {
            this.entity = entity;
        }
    }
}
