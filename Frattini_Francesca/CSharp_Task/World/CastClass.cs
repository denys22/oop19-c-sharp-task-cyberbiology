﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.World;
using Model.Entities;
using static Model.Entities.Entity;

namespace Model.World
{
    public class CastClass
    { 
        public Cell CellCast(Entity entity)
        {
            if (entity is Cell) {
                return (Cell)entity;
            }
            throw new InvalidOperationException("not a cell");
        }
        public Stone StoneCast(Entity entity)
        {
            if (entity.GetEntityType() == EntityTypeNameEnum.STONE)
            {
                return (Stone)entity;
            }
            throw new InvalidOperationException("not a stone");
        }
        public CellDead CellDeadCast(Entity entity)
        {
            if (entity is CellDead) {
                return (CellDead)entity;
            }
            throw new InvalidOperationException("not a dead cell");
        }

        public CellAlive CellAliveCast(Entity entity)
        {
            if (entity is CellAlive) {
                return (CellAlive)entity;
            }
            throw new InvalidOperationException("not an alive cell");
        }
    }
}
