﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Code_From_Grushchack_Denys;
using Model.Entities;
using static Model.Entities.Cell;
using static Model.Entities.Entity;

namespace Model.World
{
    public interface IWorld
    {
        /**
        * <returns>height of the world</returns>
        */
        int GetWorldHeight();

        /**
         * <returns>width of the world</returns>
         */
        int GetWorldWidth();

        /**
         * <returns>the map containing all the cells</returns>
         */
        Dictionary<ValueTuple<int, int>, Square> GetMap();

        /**
         * create a new dead cell in the given position.
         * <param name="x">width</param>
         * <param name="y"> height</param>
         */
        void MakeCellDeath(int x, int y);

        /**
         * <param name="x"> width</param>
         * <param name="y"> height</param>
         * <returns> the square at the given position</returns>
         */
        Square GetSquare(int x, int y);

        /**
         * <returns> number of alive cells in all the simulation</returns>
         */
        int GetAliveCells();

        /**
         * <returns> number of dead cells in all the simulation</returns>
         */
        int GetDeadCells();

        /**
         * free the square at the given position.
         * <param name="x"> width</param>
         * <param name="y"> height</param>
         */
        void RemoveCell(int x, int y);

        /**
         * move the cell in the given direction and update its position
         * if the position is already occupated, method is aborted.
         * <param name="x"> width</param>
         * <param name="y"> height</param>
         * <param name="direction"> direction where move the cell</param>
         */
        void TryMoveCell(int x, int y, Direction direction);

        /**
         * check if there's at least one position free around the cell we want to move.
         * <param name="x"> width</param>
         * <param name="y"> height</param>
         * <returns>TRUE if there's at least one position free around the cell, FALSE otherwise</returns>
         */
        bool IsThereFreeSpaceAround(int x, int y);

        /**
         * add the child of the cell in position (x, y) in the given direction, 
         * if the position is already occuped, it puts the cell in another place around the mother cell, if there's 
         * something free.
         * <param name="x"> width</param>
         * <param name="y"> height</param>
         * <param name="cell"> cell to insert</param>
         * <param name="direction"> direction to move the cell</param>
         */
        void PutChild(int x, int y, Cell cell, Direction direction);

        /**
         * <param name="x"> width</param>
         * <param name="y"> height</param>
         * <returns the type of the entity> (cell or stone) in the given position (live or dead)</returns>
         */
        EntityTypeNameEnum GetType(int x, int y);

        /** 
         * <param name="x"> width</param>
         * <param name="y"> height</param>
         * <returns>the cell type of the square in the given position</returns>
         */
        CellTypeNameEnum GetCellType(int x, int y);

        /**
         * put a cell in the given postion, if it's free.
         * <param name="x"> width</param>
         * <param name="y"> height</param>
         * <param name="cell"> cell to insert</param>
         */
        void PutCell(int x, int y, Entity cell);

        /**
         * <param name="x"> width</param>
         * <param name="y"> height</param>
         *  <returns> true if the square in the given position is occuped by an entity, false otherwise</returns>
         */
        bool IsThereAnything(int x, int y);

    }
}
