﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Code_From_Grushchack_Denys;
using Model.Entities;
using Model.Utils;

namespace Model.World
{
    public class Gravity : IGravity
    {
        private readonly World screen;

        private readonly Direction direction = Direction.SOUTH;

        public Gravity(World world)
        {
            this.screen = world;
        }

        public void CellFallingDown(CellDead cell)
        {
            this.TryMoveCell(cell.GetX(), cell.GetY(), direction, cell);
        }

        private void TryMoveCell(int x, int y, Direction direction, CellDead cell)
        {
            int w = x + direction.MovementAlongX();
            int h = y + direction.MovementAlongY();
            if (!screen.IsThereAnything(w, h))
            {
                screen.GetSquare(w, h).SetEntity(Optional<Entity>.Of(cell));
                screen.GetSquare(x, y).SetEntity(Optional<Entity>.Empty());
                cell.SetX(w);
                cell.SetY(h);
            }
        }
    }
}
