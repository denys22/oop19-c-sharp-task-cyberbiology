﻿#nullable enable

using Model.Code_From_Grushchack_Denys;
using System;
using Model.Entities;
using Model.Utils;
using static Model.Entities.Entity;
using static Model.Entities.Cell;
using System.Collections.Generic;

namespace Model.World
{
    public class World : IWorld
    {
        /**
        * used to cast classes.
        */
        private readonly CastClass cast = new CastClass();

        /**
        * the map that contains the grid with all the cells.
        */
        private Dictionary<ValueTuple<int, int>, Square> map = new Dictionary<ValueTuple<int, int>, Square>();
        /**
         * tracks number of alive cells in all the world.
         */
        private int counterAliveCells = 0;

        /**
         * tracks number of death cells in all the world.
         */
        private int counterDeadCells = 0;

        /**
         * size of the world.
         */
         private readonly Pair<int, int> size;

        /**
         * start the world, add stones in the height borders and add some cells in random positions.
         * @param height world height
         * @param width world 
         */
        public World(int width, int height)
        {
            this.size = new Pair<int, int>(width, height);
            for (int w = 0; w < this.size.GetX(); w++)
            {
                for (int h = 0; h < this.size.GetY(); h++)
                {
                    this.map.Add(new ValueTuple<int, int>(w, h), new Square(Optional<Entity>.Empty()));
                }
            }
            for (int w = 0; w < this.size.GetX(); w++)
            {
                Entity stone = new Stone(w, this.size.GetY() - 1);
                this.GetSquare(w, this.size.GetY() - 1).SetEntity(Optional<Entity>.Of(stone));
            }
        }

        public void MakeCellDeath(int x, int y)
        {
            Entity celldead = new CellDead(x, y);
            this.GetSquare(x, y).SetEntity(Optional<Entity>.Of(celldead));
            this.counterDeadCells++;
            this.counterAliveCells--;
        }

        public void RemoveCell(int x, int y)
        {
            if (this.IsThereAnything(x, y))
            {
                if (this.cast.CellCast(GetSquare(x, y).GetEntity().Get()) is CellDead)
                    this.counterAliveCells--;
                else
                    this.counterAliveCells--;
                this.GetSquare(x, y).SetEntity(Optional<Entity>.Empty());
            }
            else
                throw new InvalidOperationException("no cell here!");
        }

        public void TryMoveCell(int x, int y, Direction direction)
        {
            int w = x + direction.MovementAlongX();
            int h = y + direction.MovementAlongY();
            if (this.GetSquare(x, y).GetEntity().Get() is Cell)
            {
                if (w >= this.size.GetX() || w < 0)
                {
                    w = Math.Abs(Math.Abs(w) - this.size.GetX());
                }
                if (!this.map.ContainsKey(new ValueTuple<int, int>(w, h)))
                    return;
                if (!this.IsThereAnything(w, h))
                {
                    this.GetSquare(w, h).SetEntity(Optional<Entity>.Of(GetSquare(x, y).GetEntity().Get()));
                    this.cast.CellCast(this.GetSquare(w, h).GetEntity().Get()).SetX(w);
                    this.cast.CellCast(this.GetSquare(w, h).GetEntity().Get()).SetY(h);
                    this.GetSquare(x, y).SetEntity(Optional<Entity>.Empty());
                }
            }
        }

        public bool IsThereFreeSpaceAround(int x, int y)
        {
            for (int w = x - 1; w <= x + 1; w++)
            {
                for (int h = y - 1; h <= y + 1; h++)
                {
                    if (w == x && h == y)
                        continue;
                    if (h < 0)
                        continue;
                    int w1 = w;
                    if (w1 < 0)
                        w1 = this.size.GetX() - 1;
                    if (w1 >= this.size.GetX())
                        w1 = 0;
                    if (this.map.ContainsKey(new ValueTuple<int, int>(w1, h)))
                    {
                        if (!this.IsThereAnything(w1, h))
                            return true;
                    }
                }
            }
            return false;
        }

        public void PutChild(int x, int y, Cell cell, Direction direction)
        {
            if (!this.IsThereAnything(x, y))
                throw new InvalidOperationException("no cell mother");
            int w = x + direction.MovementAlongX();
            if (w > this.size.GetX() - 1 || w < 0)
                w = Math.Abs(Math.Abs(w) - this.size.GetX());
            int h = y + direction.MovementAlongY();
            if (h >= this.size.GetY() - 1 || h < 0)
            {
                ValueTuple<int, int> p = this.SpaceAroundCell(x, y);
                if (p != (-1, -1) && this.map.ContainsKey(p))
                {
                    if (this.map.TryGetValue(p, out Square value))
                    {
                        value.SetEntity(Optional<Entity>.Of(cell));
                        this.counterAliveCells++;
                        cell.SetX(p.Item1);
                        cell.SetY(p.Item2);
                    }
                }
                return;
            }
            if (!this.IsThereAnything(w, h))
            {
                this.GetSquare(w, h).SetEntity(Optional<Entity>.Of(cell));
                cell.SetX(w);
                cell.SetY(h);
                this.counterAliveCells++;
            }
            else
            {
                ValueTuple<int, int> p = this.SpaceAroundCell(x, y);
                if (p != (-1, -1) && this.map.ContainsKey(p))
                {
                    if (this.map.TryGetValue(p, out Square? s))
                    {
                        s.SetEntity(Optional<Entity>.Of(cell));
                        this.counterAliveCells++;
                        cell.SetX(p.Item1);
                        cell.SetY(p.Item2);
                    }
                }
            }
        }

        public EntityTypeNameEnum GetType(int x, int y)
        {
            if (this.IsThereAnything(x, y))
                return this.GetSquare(x, y).GetEntity().Get().GetEntityType();
            throw new InvalidOperationException("not an entity!");
        }

        public CellTypeNameEnum GetCellType(int x, int y)
        {
            if (this.GetType(x, y) == EntityTypeNameEnum.CELL)
                return cast.CellCast(this.GetSquare(x, y).GetEntity().Get()).GetCellTypeName();
            throw new InvalidOperationException("not a cell!");
        }

        private ValueTuple<int, int> SpaceAroundCell(int x, int y)
        {
            for (int w = x - 1; w <= x + 1; w++)
            {
                for (int h = y - 1; h <= y + 1; h++)
                {
                    if (w == x && h == y)
                        continue;
                    int w1 = w;
                    if (w1 < 0)
                        w1 = this.size.GetX() - 1;
                    else if (w1 >= this.size.GetX())
                        w1 = 0;
                    if (h >= this.size.GetY() - 1 || h < 0)
                        continue;
                    if (this.map.ContainsKey(new ValueTuple<int, int>(w1, h)))
                    {
                        if (!this.IsThereAnything(w1, h))
                            return new ValueTuple<int, int>(w1, h);
                    }
                }
            }
            return new ValueTuple<int, int>(-1, -1);
        }

        public int GetWorldHeight()
        {
            return this.size.GetY();
        }

        public int GetWorldWidth()
        {
            return this.size.GetX();
        }

        public Square GetSquare(int x, int y)
        {
            if (y < this.size.GetY() && y >= 0)
            {
                if (x >= this.size.GetX() || x < 0) 
                    x = Math.Abs(Math.Abs(x) - this.size.GetX());
                if (this.map.TryGetValue(new ValueTuple<int, int>(x, y), out Square square))
                    return square;
            }
            throw new InvalidOperationException("no square in this position!");
        }

        public int GetAliveCells()
        {
            return this.counterAliveCells;
        }

        public int GetDeadCells()
        {
            return this.counterDeadCells;
        }

        public void PutCell(int x, int y, Entity cell)
        {
            if (!this.IsThereAnything(x, y))
            {
                this.GetSquare(x, y).SetEntity(Optional<Entity>.Of(cell));
                Console.WriteLine(this.GetSquare(x, y).GetEntity().Get());
                if (cell.GetEntityType().Equals(EntityTypeNameEnum.STONE))
                    return;
                if (cast.CellCast(cell).GetCellTypeName().Equals(CellTypeNameEnum.CELL_DEAD))
                    this.counterDeadCells++;
                else if (cast.CellCast(cell).GetCellTypeName().Equals(CellTypeNameEnum.CELL_STANDARD_ALIVE))
                    this.counterAliveCells++;
                Console.WriteLine(cell.GetX() + " " +cell.GetY() + " "+this.GetSquare(x, y).GetEntity().IsPresent);

            }
            else
                throw new InvalidOperationException("position already taken");
        }

        public bool IsThereAnything(int x, int y)
        {
            return this.GetSquare(x, y).GetEntity().IsPresent;
        }

        public Dictionary<(int, int), Square> GetMap()
        {
            return this.map;
        }
    }
}
