﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Entities;

namespace Model.Entities
{
    public class CellDead : Cell
    {
        /**
    * a classic constructor.
    * @param x
    * the x.
    * @param y
    * the y.
    */
        public CellDead(int x, int y)
        :base (x, y) {  }
    public override sealed CellTypeNameEnum GetCellTypeName()
        {
            return CellTypeNameEnum.CELL_DEAD;
        }

    public new void SetX(int x)
        {
            base.SetX(x);
        }
    public new void SetY(int y)
        {
            base.SetY(y);
        }
    }
}
