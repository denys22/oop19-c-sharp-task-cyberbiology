﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Entities;

namespace Model.Entities
{
    public class Stone : Entity
    {
        /**
         * standard constructor to place the stone in the world.
         * @param x coordinate.
         * @param y coordinate.
         */
        public Stone(int x, int y)
                : base(x, y) { }
        
    public override EntityTypeNameEnum GetEntityType()
        {
            return EntityTypeNameEnum.STONE;
        }
    }
}

