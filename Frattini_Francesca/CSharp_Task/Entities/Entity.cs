﻿#nullable enable
using Model.Entities;


namespace Model.Entities
{
    public abstract class Entity 
    {
        /**
     * the x of the entity.
     */
        private int x;
        /**
         * the x of the entity.
         */
        private int y;
        /**
         * 
         * @param x              the x of the entity
         * @param y              the y of the entity
         */
        public Entity(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        /**
         * @return the x
         */
        public int GetX()
        {
            return this.x;
        }

        /**
         * @return the y
         */
        public int GetY()
        {
            return this.y;
        }
        /**
         * @param x the x to set
         */
        protected void SetX(int x)
        {
            this.x = x;
        }

        /**
         * @param y the y to set
         */
        protected void SetY(int y)
        {
            this.y = y;
        }

        /**
         * @return the entityTypeName
         */
        public abstract EntityTypeNameEnum GetEntityType();

        public enum EntityTypeNameEnum 
        {

            /**
            * cell, in turn it can be of different types.
            */
            CELL,
            /**
            * a stone.
            * a immortal and innamovible object
            */
            STONE,
        }
    }
}