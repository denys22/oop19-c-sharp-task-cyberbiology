﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Entities;

namespace Model.Entities
{
    public abstract class Cell : Entity
    {
        /**
        * 
        * @param x
        * the x.
        * @param y
        * the y.
        */
        protected Cell(int x, int y)
        : base (x, y) { }

    public override EntityTypeNameEnum GetEntityType()
        {
            return EntityTypeNameEnum.CELL;
        }
        /**
         * a standard setter.
         * @param x coordinate.
         */
        public new void SetX(int x)
        {
            base.SetX(x);
        }
        /**
         * a standard setter.
         * @param y coordinate.
         */
        public new void SetY(int y)
        {
            base.SetY(y);
         }

        public abstract CellTypeNameEnum GetCellTypeName();
        public enum CellTypeNameEnum
        {
            /**
             * the standard (and only) cell of the cell type.
             */
            CELL_STANDARD_ALIVE,
            /**
             * the dead cell with no active method.
             */
            CELL_DEAD
        }

    }
}
