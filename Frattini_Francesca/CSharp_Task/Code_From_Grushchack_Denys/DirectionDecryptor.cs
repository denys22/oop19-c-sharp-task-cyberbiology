﻿namespace Model.Code_From_Grushchack_Denys
{
    /**
     * 
     * <summary>A classe for decryption of integers into directions.</summary>
     *
     */
    public static class DirectionDecryptor
    {
        /**
         * <summary>Transform an integer into direction.</summary>
         * <param name="parameter"> parameter which must be transformed in a direction.</param>
         * <returns> a direction that corresponds to the parameter.</returns>
         */
        public static Direction GetDirection(in int parameter)
        {
            if (parameter < 0)
            {
                return DirectionExtention.GetDirection(((parameter % DirectionExtention.Size) 
                                                        + DirectionExtention.Size) % DirectionExtention.Size);
            }
            return DirectionExtention.GetDirection(parameter % DirectionExtention.Size);
        }
    }
}

