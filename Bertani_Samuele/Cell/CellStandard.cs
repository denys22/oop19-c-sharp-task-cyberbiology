﻿using model.entity.cell.standard.age;
using model.entity.cell.standard.genome;
using model.entity.cell.standard.obtainable;

namespace model.entity.cell.standard
{


	/// 
	/// <summary>
	/// the standard interface for the main protagonist of the game of life.
	/// 
	/// </summary>
	/// <summary>
	/// core interface for cell standard methods.
	/// if u dont see a method, try in extended interfaces.
	/// 
	/// </summary>
	public interface ICellStandard : IGenome, IAge, Obtainable
	{

		/// <summary>
		/// the main method . start the cell work with gene.
		/// </summary>
		void run();
		/// <summary>
		/// hashcode.
		/// @return
		/// the hashcode.
		/// </summary>
		int GetHashCode();
		/// 
		/// <summary>
		/// @return
		/// the class to string for debug.
		/// </summary>
		string ToString();
	}

}