﻿
using System;
using System.Collections.Generic;
using model.entity.cell.standard.age;
using model.entity.cell.standard.genome;
using model.entity.cell.standard.obtainable;

namespace model.entity.cell.standard
{
    /// 
    /// <summary>
    /// the core implementation of this game.
    /// all the cell data and action.
    /// 
    /// </summary>
    public class CellStandardImpl : ICellStandard
    {
        /// <summary>
        /// the age of the cell.
        /// </summary>
        private readonly IAgeManipulation age;
        /// <summary>
        /// the GENOME.
        /// 
        /// a class with a method for genes manipulation and all genes data.
        /// </summary>
        private readonly IGenomeManipulation genome;
        /// <summary>
        /// the energy of the cell.
        /// </summary>
        private int energy;
        /// <summary>
        /// the mineral of the cell.
        /// </summary>
        private int mineral;
        private bool active = true;

        /// the direction of the cell. </param>
        /// <param name="energy">
        /// the energy of the start </param>
        /// <param name="mineral">
        /// the minerals in the start </param>
        /// <param name="genome">
        /// the genome of the cell with gene </param>
        public CellStandardImpl(int energy, int mineral, IList<int> genome)
        {
            this.energy = energy;
            this.mineral = mineral;
            this.age = new age.AgeImpl(100); //TODO: magic number
            this.genome = new GenomeImpl(genome);
        }

        public void run()
        {
            this.check();
            // age
            this.age.increment();
            if (this.age.Dead)
            {
                suicide();
            }
            else
            {
                // reproduce
                if (this.energy == 1000) //TODO magic number 
                {
                    Console.WriteLine("i tryed to reproduce");
                }
                // energy and gene
                if (energyDrain())
                {
                    Console.WriteLine("i tryed to pick mineral");
                    this.genome.startGene(this);
                }

            }
        }

        private void suicide()
        {
            Console.WriteLine("i tryed to suicide");
        }

        private bool energyDrain()
        {
            this.energy -= 10; //TODO: magic number
            if (this.energy < 0)
            {
                suicide();
                return false;
            }
            return true;
        }


        // ______________________________________________________________
        // GENOME
        // ______________________________________________________________

        public void mutateGene(int index, int gene)
        {
            this.check();
            this.genome.mutateGene(index, gene);
        }

        public int getGeneValue(int index)
        {
            return this.genome.getGeneValue(index);
        }

        public int getGeneIndex(int value)
        {
            return this.genome.getGeneIndex(value);
        }

        public int getGeneValueWithOffsetAndJump(int offset)
        {
            return this.genome.getGeneValueWithOffsetAndJump(offset);
        }

        public void setGene(int index, int value)
        {
            this.check();
            this.genome.setGene(index, value);
        }

        public int Age
        {
            get
            {
                return this.age.Age;
            }
        }

        public int NumberOfGenes
        {
            get
            {
                return this.genome.NumberOfGenes;
            }
        }

        public IList<int> Genome
        {
            get
            {
                return this.genome.Genome;
            }
        }


        public void changeSideLength(int value)
        {
            this.genome.changeSideLength(value);
        }

        public void decrementEnergy(int value)
        {
            this.check();
            if (value < 0)
            {
                throw new System.ArgumentException();
            }
            this.energy -= value;
            if (this.energy < 0)
            {
                this.energy = 0;
            }
        }

        public void decrementMineral(int value)
        {
            this.check();
            if (value < 0)
            {
                throw new System.ArgumentException();
            }
            this.mineral -= value;
            if (this.mineral < 0)
            {
                this.mineral = 0;
            }
        }

		private void check() {
			Console.WriteLine("i am useless :(");
		}
        public int Mineral
        {
            get
            {
                return this.mineral;
            }
        }
        public int Energy
        {
            get
            {
                return this.energy;
            }
        }

        public bool Active => throw new NotImplementedException();

        public void incrementMineral(int value)
        {
            this.check();
            if (value < 0)
            {
                throw new System.ArgumentException();
            }
            this.mineral += value;
            if (this.mineral >= 100)
            {
                this.mineral = 100;
            }
        }

        
        public void incrementEnergy(int value, EnergyTypeEnum energyType)
        {
            this.check();
            if (value < 0)
            {
                throw new System.ArgumentException();
            }
            this.energy += value;
            if (this.energy >= 100)
            {
                this.energy = 100;
            }
        }

        // ______________________________________________________________
        // AGE
        // ______________________________________________________________
        public void resetAge()
        {
            this.age.resetAge();
        }

    }
}

