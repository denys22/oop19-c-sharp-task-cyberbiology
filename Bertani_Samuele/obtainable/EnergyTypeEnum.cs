﻿namespace model.entity.cell.standard.obtainable
{
	/// 
	/// <summary>
	/// the motivation for the energy acquisition of a cell.
	/// This serves to catologize the behaviour of the cellules according to their feeding habits.
	/// 
	/// </summary>
	public enum EnergyTypeEnum
	{
		/// <summary>
		/// eating another cell.
		/// </summary>
		EATING,
		/// <summary>
		/// gaining energy from light.
		/// </summary>
		PHOTOSYNTHESIS,
		/// <summary>
		/// by transforming minerals into energy through a specific and mysterious gene.
		/// </summary>
		CONVERTING_MINERAL,
		/// <summary>
		/// gifted for other near cells.
		/// </summary>
		ALTRUISM
	}

}