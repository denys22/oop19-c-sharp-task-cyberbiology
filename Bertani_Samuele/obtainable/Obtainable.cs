﻿namespace model.entity.cell.standard.obtainable
{

	/// 
	/// <summary>
	/// All method to change the obtainable of the cell (energy and mineral).
	/// 
	/// </summary>
	public interface Obtainable
	{
		/// 
		/// <param name="value">
		/// the value of decrementation.
		/// </param>
		/// <exception cref="IllegalArgumentException"> if value is negative
		/// 
		/// set to 0 if the value is bigger of the energy of the cell. </exception>
		void decrementEnergy(int value);
		/// 
		/// <param name="value">
		/// the value of decrementation.
		/// </param>
		/// <exception cref="IllegalArgumentException"> if value is negative
		/// 
		/// set to 0 if the value is bigger of the mineral of the cell. </exception>
		void decrementMineral(int value);
		/// 
		/// <summary>
		/// a standard getter.
		/// </summary>
		/// <returns> energy </returns>
		int Energy {get;}

		/// 
		/// <summary>
		/// a standard getter.
		/// </summary>
		/// <returns> mineral </returns>
		int Mineral {get;}

		/// <summary>
		/// add or subtract (with a MAX and a MIN(0) ) to energy.
		/// </summary>
		/// <param name="value">      an int to add (or subtract) to the energy of the cell. </param>
		/// <param name="energyType"> the type of the energy added. </param>
		/// <exception cref="IllegalArgumentException"> if value is negative </exception>
		void incrementEnergy(int value, EnergyTypeEnum energyType);

		/// <summary>
		/// add or subtract (with a MAX and a MIN(0) ) to mineral.
		/// </summary>
		/// <param name="value"> an int to add (or subtract) to the mineral of the cell. </param>
		/// <exception cref="IllegalArgumentException"> if value is negative. </exception>
		void incrementMineral(int value);
	}

}