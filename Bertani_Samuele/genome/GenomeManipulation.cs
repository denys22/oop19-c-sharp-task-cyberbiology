﻿namespace model.entity.cell.standard.genome
{

	/// <summary>
	/// interface used for gene manipulation.
	/// 
	/// </summary>
	public interface IGenomeManipulation : IGenome
	{
		/// <summary>
		/// Generate a random int in the genome.
		/// </summary>
		/// <returns> a int in the range of gene;
		///  </returns>
		int generateRandomGene();

		/// <summary>
		/// start with the current gene. add 1 to count. refresh if out of bound.
		/// </summary>
		/// <param name="currentCell"> the current cell that call the gene. </param>
		void startGene(ICellStandard currentCell);

		/// 
		/// <returns> the size of the genome </returns>
		int Size {get;}
		/// 
		/// <param name="value">
		/// the new value of current pointer </param>
		int Current {set;get;}

	}
 
}