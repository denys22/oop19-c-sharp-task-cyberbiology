﻿using System.Collections.Generic;

namespace model.entity.cell.standard.genome
{

	/// 
	/// <summary>
	/// the standard interface for the manipulation of the gene integrated in the cell.
	/// 
	/// </summary>
	public interface IGenome
	{
		/// <summary>
		/// is a standard set. </summary>
		/// <param name="index">
		/// the index of the gene </param>
		/// <param name="gene">
		/// the new gene </param>
		void mutateGene(int index, int gene);
		/// <summary>
		/// get the gene int of a specific index.
		/// </summary>
		/// <param name="index"> a standard integer (is a circular array) </param>
		/// <returns> the value of the gene </returns>
		int getGeneValue(int index);
		/// 
		/// <param name="value"> the value of the gene </param>
		/// <returns> -1 if there isnt a gene with the value in the genome index of the
		///         first gene with the value in the genome </returns>
		int getGeneIndex(int value);
		/// 
		/// <param name="offset"> a int for the "Jump" </param>
		/// <returns> the value of the gene index </returns>
		int getGeneValueWithOffsetAndJump(int offset);
		/// 
		/// <param name="index"> index of genome </param>
		/// <param name="value"> value of the gene
		///  </param>
		void setGene(int index, int value);
		/// <summary>
		/// a getter.
		/// @return
		/// a int of the numbers of type of genes
		/// </summary>
		int NumberOfGenes {get;}
		/// 
		/// <summary>
		/// @return
		/// the genome list
		/// </summary>
		IList<int> Genome {get;}
		/// <summary>
		/// this method change the length of the genome and help with some error.
		/// </summary>
		/// <param name="value"> the change of the length of the genome </param>
		void changeSideLength(int value);


	}

}