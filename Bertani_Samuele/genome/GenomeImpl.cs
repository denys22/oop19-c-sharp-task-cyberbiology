﻿using System;
using System.Collections.Generic;

namespace model.entity.cell.standard.genome
{

	/// 
	/// <summary>
	/// Methods the cell can use to work on genome.
	/// 
	/// </summary>
	public class GenomeImpl : IGenomeManipulation
	{
		/// <summary>
		/// the genome list. i choose a linked list because i assume that there is a lot
		/// of mutation.
		/// </summary>
		private readonly IList<int> genome = new List<int>();
		/// <summary>
		/// pointer of current gene.
		/// </summary>
		private int current = 0;
		/// <summary>
		/// random for some function.
		/// </summary>
		private readonly Random rand = new Random();
		private readonly int numberOfGenes;



		/// <summary>
		/// Standard manufacturer that requires the genome list and various information to decrypt genes. </summary>
		/// <param name="numberOfGenes"> number of genes. </param>
		/// <param name="list"> a list with indexes of genes. </param>
		public GenomeImpl(IList<int> list)
		{
			this.numberOfGenes = 10;

			for (int i = 0; i < list.Count; i++)
			{
				this.genome.Add(list[i]);
			}
		}

		public int generateRandomGene()
		{
			return rand.Next(this.numberOfGenes);
		}


		public void changeSideLength(int value)
		{
			if (value >= 0)
			{
				for (int i = 0; i < value; i++)
				{
					this.genome.Add(generateRandomGene());
				}
			}
			else
			{
				if (this.genome.Count + value < 1)
				{
					throw new System.IndexOutOfRangeException();
				}
				else
				{
					for (int i = 0; i < -value; i++)
					{
						this.genome.RemoveAt(this.genome.Count - 1);
					}
				}
			}

		}


		public void startGene(ICellStandard currentCell)
		{
			Console.WriteLine("i try to use a gene");
		}


		public int getGeneValue(int index)
		{
			return this.genome[index];
		}


		public int getGeneIndex(int value)
		{
			return this.genome.IndexOf(value);
		}

		public void setGene(int index, int value)
		{
			this.genome[index] = value;
		}


		public int getGeneValueWithOffsetAndJump(int offset)
		{
			this.current += offset;
			return this.genome[this.current];
		}

		public int Size
		{
			get
			{
				return this.genome.Count;
			}
		}

		public void mutateGene(int index, int gene)
		{
			this.genome[index] = gene;
		}

		public int NumberOfGenes
		{
			get
			{
				return this.numberOfGenes;
			}
		}


		public int Current
		{
			set
			{
				if (value < 0)
				{
					throw new System.ArgumentException();
				}
				else
				{
					this.current = value;
				}
			}
			get
			{
				return this.current;
			}
		}


		public IList<int> Genome
		{
			get
			{
				return this.genome;
			}
		}

	}

}