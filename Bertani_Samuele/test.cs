using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using model.entity.cell.standard;
using model.entity.cell.standard.age;
using model.entity.cell.standard.obtainable;

namespace BankTests
{
    [TestClass]
    public class CellStandardTest
    {

        [TestMethod]
        public void TestIAge()
        {
            //ICellStandard cell = createCell();
            IAgeManipulation age = new AgeImpl(100);
            // starting age
            Assert.AreEqual(age.Age, 0);

            // adding age
            age.increment();
            Assert.AreEqual(age.Age, 1);

            // testing reset
            age.resetAge();
            Assert.AreEqual(age.Age, 0);

            // testing die
            for (int i = 0; i < 100; i++)
            {
                age.increment();
                Assert.AreEqual(age.Dead, false);
            }
            age.increment();
            Assert.AreEqual(age.Dead, true);
            age.increment();
            Assert.AreEqual(age.Dead, true);

        }

        [TestMethod]
        public void TestICell()
        {
            ICellStandard cell = createCell();

            cell.incrementEnergy(50, EnergyTypeEnum.EATING);
            Assert.AreEqual(cell.Energy, 100);

            cell.incrementEnergy(50, EnergyTypeEnum.EATING);
            Assert.AreEqual(cell.Energy, 100);

            cell.decrementEnergy(50);
            Assert.AreEqual(cell.Energy, 50);

            cell.decrementEnergy(100);
            Assert.AreEqual(cell.Energy, 0);

            cell.incrementMineral(50);
            Assert.AreEqual(cell.Mineral, 100);

            cell.incrementMineral(50);
            Assert.AreEqual(cell.Mineral, 100);

            cell.decrementMineral(50);
            Assert.AreEqual(cell.Mineral, 50);

            cell.decrementMineral(100);
            Assert.AreEqual(cell.Mineral, 0);
        }

        [TestMethod]
        public void TestIgenome()
        {
            ICellStandard cell = createCell();

            Assert.AreEqual(cell.getGeneValue(1), 1);

            cell.setGene( 1,  6);
            Assert.AreEqual(cell.getGeneValue(1), 6);

            Assert.AreEqual(cell.getGeneIndex(6), 1);
            
            
        }

        private ICellStandard createCell()
        {
            IList<int> genome = new List<int>();
            genome.Add(0);
            genome.Add(1);
            genome.Add(2);
            genome.Add(3);
            ICellStandard cell = new CellStandardImpl(50, 50, genome);
            return cell;
        }
    }
}