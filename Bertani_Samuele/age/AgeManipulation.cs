﻿namespace model.entity.cell.standard.age
{
	/// 
	/// <summary>
	///  Methods the cell can use to work on age.
	/// 
	/// </summary>
	public interface IAgeManipulation : IAge
	{

		/// <summary>
		/// increment with a +1.
		/// </summary>
		void increment();
		/// 
		/// <returns> <code>true</code> if is dead for old age. </returns>
		bool Dead {get;}

	}

}