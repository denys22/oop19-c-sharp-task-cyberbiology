﻿namespace model.entity.cell.standard.age
{
	/// 
	/// <summary>
	/// The age of the cell (number of cycle of the created cell).
	/// 
	/// </summary>
	public interface IAge
	{
		/// <summary>
		/// a simple get. </summary>
		/// <returns> the age </returns>
		int Age {get;}
		/// <summary>
		///reset the age of the cell.
		/// 
		/// </summary>
		void resetAge();
	}

}