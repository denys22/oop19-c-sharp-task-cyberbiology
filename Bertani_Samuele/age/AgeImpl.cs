﻿namespace model.entity.cell.standard.age
{
	/// 
	/// <summary>
	/// All method and fields for the cell age.
	/// 
	/// </summary>
	public class AgeImpl : IAgeManipulation
	{
		/// <summary>
		/// the age of the cell.
		/// </summary>
		private int age;
		/// <summary>
		/// the max age of the cell.
		/// </summary>
		private readonly int maxAge;

		/// <param name="maxAge"> the maximum cell age. </param>
		public AgeImpl(int maxAge)
		{
			this.maxAge = maxAge;
		}

		public int Age
		{
			get
			{
				return this.age;
			}
		}

		public void increment()
		{
			this.age++;
		}

		public bool Dead
		{
			get
			{
				return this.age > this.maxAge;
			}
		}

		public void resetAge()
		{
			this.age = 0;
		}

	}

}